from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
# from flask_sock import Sock
from flask_socketio import SocketIO
from flask_socketio import send, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
CORS(app)
# sock = Sock(app)


# Create some test data for our catalog in the form of a list of dictionaries.
users = [
    {
        "id": 1,
        "username": "jorge",
        "password": "123",
        "chats": [
            {
                "id": 2,
                "name": "wilhelm",
                "messages": [
                    {
                        "txt": "hola",
                    },
                ],
            },
        ]
    },
    {
        "id": 2,
        "username": "wilhelm",
        "password": "123",
        "chats": [
            {
                "id": 1,
                "name": "jorge",
                "messages": [
                    {
                        "txt": "hola2",
                    },
                ],
            },
        ]
    },
]


# @sock.route('/reverse')
# def reverse(ws):
#     while True:
#         text = ws.receive()
#         ws.send(text)


# @app.route('/')
# def index():
#     return render_template('index.html')


# @app.route('/view')
# def view():
#     return render_template('view.html')


@socketio.on('message')
def handle_message(data):
    print('received message: ' + data)


@socketio.on('connect')
def test_connect():
    emit('my response', {'data': 'Connected'})


@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected')


# login endpoint


@app.route('/api/login/', methods=['POST'])
def login():
    userRequest = {
        'username': request.json['username'],
        'password': request.json['password'],
    }
    userFound = [
        user for user in users if user['username'] == userRequest['username'].lower() and user['password'] == userRequest['password'].lower()]
    if (len(userFound) > 0):
        return jsonify({
            'user': userFound[0]
        })
    return jsonify({'message': 'User Not found'})

# Create Data Routes


@ app.route('/api/messages/<string:userId>/', methods=['POST'])
def addMessage(userId):
    to = request.json['to']
    new_message = {
        'txt': request.json['txt'],
    }
    userFound = [
        user for user in users if user['id'] == int(userId)]
    toFound = [
        user for user in users if user['id'] == int(to)]
    if (len(userFound) > 0):
        chat = [
            chat for chat in userFound[0]['chats'] if chat['id'] == int(to)]
        toChat = [
            chat for chat in toFound[0]['chats'] if chat['id'] == int(userId)]
        print(toChat)
        chat[0]['messages'].append(new_message)
        toChat[0]['messages'].append(new_message)

        return jsonify({'messages': chat[0]['messages']})
    return jsonify({'message': 'Error'})


if __name__ == '__main__':
    socketio.run(app)
    # app.run(debug=True, port=5000)
